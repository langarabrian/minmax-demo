var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    const pid=process.pid;
    const start = process.hrtime();
    var min = Math.random();
    var max = min;
    var i, next;
    for ( i=0; i<100000000; i++ ) {
        next = Math.random();
        if ( next < min ) min = next;
        if ( next > max ) max = next;
    }
    const diff = process.hrtime(start);
    res.render('minmax', { title: 'Express', min, max, diff, pid });
});

module.exports = router;
