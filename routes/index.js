var express = require('express');
var router = express.Router();
const os = require('os');

/* GET home page. */
router.get('/', function(req, res, next) {
    const hostname = os.hostname();
    const cwd = process.cwd();
    res.render('index', { title: 'Express', hostname, cwd });
});

module.exports = router;
