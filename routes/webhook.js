var express = require('express');
var router = express.Router();
const { spawn } = require('child_process');

/* POST home page. */
router.post('/', function(req, res, next) {
    console.log( req.body );
    if ( process.env.WEBHOOK_TOKEN == req.headers['x-gitlab-token'] ) {
        const pathParts = req.body['project']['path_with_namespace'].split('/');
        const projectSlug = pathParts[1];
        const baseURL = req.body['project']['web_url'];
        const scriptPath = process.env.HOME +'/' + projectSlug + '-master/update.sh'
        const subprocess = spawn( scriptPath, [baseURL, projectSlug], {
            detached: true,
            stdio: 'ignore'
        });
        subprocess.unref();
    }
    res.render('webhook', { title: 'Express: webhook POST' });
});

module.exports = router;
